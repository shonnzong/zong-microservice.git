package console

import (
	"fmt"
	"zong-microservice/pkg/scheduler"
)

// Commands 任务调度器
func Commands() {
	fmt.Println("任务调度器开始...")
	e := scheduler.Engine{
		Scheduler: &scheduler.QueueScheduler{},
		WorkCount: 10, // 并发10个协程
	}
	e.Run(scheduler.Request{
		ParseFunc: []func(){
			sendWs,
		},
	})
}
