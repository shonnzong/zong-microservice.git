package models

import (
	"gorm.io/gorm"
	"zong-microservice/pkg/database"
	"zong-microservice/pkg/logger"
)

// File 文件模型
type File struct {
	BaseModel

	FileSha1 string `gorm:"column:file_sha1;type:char(40);not null;unique;"`
	FileName string `gorm:"column:file_name;type:varchar(256);not null;"`
	FileSize int64  `gorm:"column:file_size;type:bigint(20);default:0;"`
	FileAddr string `gorm:"column:file_addr;type:varchar(100);not null;"`
	Status   int    `gorm:"column:status;type:tinyint(1);default:0;index;"`
	Ext1     int    `gorm:"column:ext1;type:int(11);default:0;"`
	Ext2     string `gorm:"column:ext2;type:text;"`
}

// Create 创建
func (FileModel *File) Create() error {
	if err := database.DB.Model(FileModel).Create(&FileModel).Error; err != nil {
		logger.LogError(err)
		return err
	}

	return nil
}

// Get 通过 file_sha1 获取数据
func (FileModel File) GetByFileSha1(file_sha1 string) (File, error) {
	if err := database.DB.Model(FileModel).Where("file_sha1 =?", file_sha1).First(&FileModel).Error; err != nil || err == gorm.ErrRecordNotFound {
		return FileModel, err
	} else {
		return FileModel, nil
	}
}
