package models

import (
	"gorm.io/gorm"
	"time"
	"zong-microservice/pkg/helper"
	"zong-microservice/pkg/logger"
)

// BaseModel 模型基类
type BaseModel struct {
	ID uint64 `gorm:"column:id;primaryKey;autoIncrement;not null"`

	CreatedAt time.Time `gorm:"column:created_at;index"`
	UpdatedAt time.Time `gorm:"column:updated_at;index"`

	// 支持 gorm 软删除
	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at" sql:"index"`
}

// GetStringID 获取 ID 的字符串格式
func (model BaseModel) GetStringID() string {
	return helper.Uint64ToString(model.ID)
}

// TimeFormat 将 string 转换成 time.Time 类型 再转化为 string
func (model BaseModel) TimeFormat(t string) string {
	formatTime, err := time.ParseInLocation("2006-01-02T15:04:05Z07:00", t, time.Local)
	logger.LogError(err)
	return formatTime.Format("2006-01-02 15:04:05")
}
