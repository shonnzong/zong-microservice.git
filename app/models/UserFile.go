package models

import (
	"zong-microservice/pkg/database"
	"zong-microservice/pkg/logger"
)

// UserFile 用户文件模型
type UserFile struct {
	BaseModel

	Username string `gorm:"column:username;type:varchar(50);not null;index;uniqueIndex:u_f;"`
	FileSha1 string `gorm:"column:file_sha1;type:char(40);not null;uniqueIndex:u_f;"`
	FileName string `gorm:"column:file_name;type:varchar(256);not null;"`
	FileSize int64  `gorm:"column:file_size;type:bigint(20);default:0;"`
	Status   int    `gorm:"column:status;type:tinyint(1);default:0;index;"`
}

// Create 创建
func (UserFileModel *UserFile) Create() error {
	if err := database.DB.Model(UserFileModel).Create(&UserFileModel).Error; err != nil {
		logger.LogError(err)
		return err
	}

	return nil
}
