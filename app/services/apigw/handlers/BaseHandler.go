package handlers

import (
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-plugins/registry/consul/v2"
	authProto "zong-microservice/app/services/auth/proto"
	userProto "zong-microservice/app/services/user/proto"
	"zong-microservice/config/consts"
)

var (
	authCli authProto.AuthService
	userCli userProto.UserService
)

func init() {
	// Register consul
	reg := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"127.0.0.1:8500",
			"127.0.0.1:8501",
			"127.0.0.1:8502",
		}
	})

	service := micro.NewService(
		micro.Flags(consts.MicroFlags...),
		micro.Registry(reg),
	)

	// Init service, 解析命令行参数等
	service.Init()

	// Init rpc client
	client := service.Client()

	// Register auth rpc client
	authCli = authProto.NewAuthService("go.micro.service.auth", client)
	userCli = userProto.NewUserService("go.micro.service.user", client)
}
