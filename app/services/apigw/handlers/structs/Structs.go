package structs

import "time"

type ResultData struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type UserData []struct {
	ID        uint64    `json:"id"`
	Username  string    `json:"username"`
	Name      string    `json:"name"`
	Phone     string    `json:"phone"`
	Avatar    string    `json:"avatar"`
	Sex       int       `json:"sex"`
	Status    int       `json:"status"`
	CreatedAt time.Time `json:"created_at"`
}
