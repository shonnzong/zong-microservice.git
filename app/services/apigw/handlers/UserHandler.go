package handlers

import (
	"context"
	request "zong-microservice/app/services/apigw/http/requests"
	userProto "zong-microservice/app/services/user/proto"
)

type UserHandler struct {
}

// RpcCreate rpc 创建用户
func (handler UserHandler) RpcCreate(request request.UserCreateRequest) (*userProto.CreateUserResponse, error) {
	return userCli.CreateUser(context.TODO(), &userProto.CreateUserRequest{
		Username: request.Username,
		Name:     request.Name,
		Phone:    request.Phone,
		Password: request.Password,
		Sex:      int32(*request.Sex),
		Status:   int32(*request.Status),
	})
}

// RpcUpdate rpc 更新用户
func (handler UserHandler) RpcUpdate(request request.UserUpdateRequest) (*userProto.UpdateUserResponse, error) {
	return userCli.UpdateUser(context.TODO(), &userProto.UpdateUserRequest{
		Id:     request.ID,
		Name:   request.Name,
		Phone:  request.Phone,
		Sex:    int32(*request.Sex),
		Status: int32(*request.Status),
	})
}

// RpcDelete rpc 删除用户
func (handler UserHandler) RpcDelete(request request.UserDeleteRequest) (*userProto.DeleteUserResponse, error) {
	return userCli.DeleteUser(context.TODO(), &userProto.DeleteUserRequest{
		Id: request.ID,
	})
}

// RpcList rpc 用户列表
func (handler UserHandler) RpcList(request request.UserIndexRequest, perPage int, page, baseUrl string) (*userProto.UserListResponse, error) {
	return userCli.UserList(context.TODO(), &userProto.UserListRequest{
		Id:       request.ID,
		Username: request.Username,
		PerPage:  int32(perPage),
		Page:     page,
		BaseUrl:  baseUrl,
	})
}

// RpcMe rpc 用户当前信息
func (handler UserHandler) RpcMe(authorization string) (*userProto.MeResponse, error) {
	return userCli.Me(context.TODO(), &userProto.MeRequest{
		AccessToken: authorization,
	})
}

// RpcUpdatePassword rpc 用户修改密码
func (handler UserHandler) RpcUpdatePassword(request request.UserPasswordRequest) (*userProto.UpdateUserPasswordResponse, error) {
	return userCli.UpdateUserPassword(context.TODO(), &userProto.UpdateUserPasswordRequest{
		Username:        request.Username,
		Password:        request.Password,
		PasswordComfirm: request.PasswordComfirm,
	})
}

// RpcDoRegister rpc 用户注册
func (handler UserHandler) RpcDoRegister(request request.UserRegisterRequest) (*userProto.DoRegisterResponse, error) {
	return userCli.DoRegister(context.TODO(), &userProto.DoRegisterRequest{
		Username:        request.Username,
		Name:            request.Name,
		Phone:           request.Phone,
		Password:        request.Password,
		PasswordComfirm: request.PasswordComfirm,
		Sex:             int32(*request.Sex),
		Status:          int32(*request.Status),
	})
}
