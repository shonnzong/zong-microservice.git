package handlers

import (
	"context"
	"zong-microservice/app/services/apigw/http/requests"
	authProto "zong-microservice/app/services/auth/proto"
)

type AuthHandler struct {
}

// RpcLogin rpc 用户登录
func (handler AuthHandler) RpcLogin(request request.AuthLoginRequest) (*authProto.LoginResponse, error) {
	return authCli.Login(context.TODO(), &authProto.LoginRequest{
		Username: request.Username,
		Password: request.Password,
	})
}

// RpcLogout rpc 用户退出
func (handler AuthHandler) RpcLogout(authorization string) (*authProto.LogoutResponse, error) {
	return authCli.Logout(context.TODO(), &authProto.LogoutRequest{
		AccessToken: authorization,
	})
}

// RpcRefresh rpc 用户刷新token
func (handler AuthHandler) RpcRefresh(authorization string) (*authProto.RefreshResponse, error) {
	return authCli.Refresh(context.TODO(), &authProto.RefreshRequest{
		AccessToken: authorization,
	})
}
