package routes

import (
	"github.com/didip/tollbooth"
	"github.com/gin-gonic/gin"
	apiController "zong-microservice/app/services/apigw/http/controllers/api"
	"zong-microservice/app/services/apigw/http/middlewares"
)

func RegisterApiRoutes(engine *gin.Engine) {
	// HTML 渲染
	engine.LoadHTMLGlob("resources/view/*")
	//engine.LoadHTMLFiles("resources/view/index.html")
	// 静态资源设置
	engine.Static("/static", "resources/static")
	// API路由设置
	api := engine.Group("/api")
	// 限流、跨域访问、JWT中间件 全局设置
	api.Use(middlewares.LimiterMiddleware(tollbooth.NewLimiter(100, nil))) // 100 请求/秒限制器.
	api.Use(middlewares.CorsMiddleware())
	// 不需要经过JWT中间件验证就能访问的接口
	api.POST("/auth", new(apiController.AuthController).Login)
	api.GET("/user", new(apiController.UserController).Index)
	api.GET("/user/register", new(apiController.UserController).Register)
	api.POST("/user/register", new(apiController.UserController).DoRegister)
	api.GET("/home", new(apiController.HomeController).Index)
	api.GET("/upload", new(apiController.UploadController).Upload)
	api.GET("/ws", new(apiController.WsController).Ws)
	// JWT中间件 全局设置
	api.Use(middlewares.JwtMiddleware())

	// JWT Bearer Token验证
	authGroup := api.Group("/auth")
	authGroup.DELETE("", new(apiController.AuthController).Logout)
	authGroup.PUT("", new(apiController.AuthController).Refresh)
	// 用户路由
	userGroup := api.Group("/user")
	userGroup.POST("", new(apiController.UserController).Create)
	userGroup.PATCH("", new(apiController.UserController).Update)
	userGroup.DELETE("", new(apiController.UserController).Delete)
	userGroup.GET("/list", new(apiController.UserController).List)
	userGroup.GET("/me", new(apiController.UserController).Me)
	userGroup.PATCH("/reset", new(apiController.UserController).UpdatePassword)
	// 上传路由
	uploadGroup := api.Group("/upload")
	uploadGroup.POST("", new(apiController.UploadController).DoUpload)
	uploadGroup.POST("/fast", new(apiController.UploadController).DoFastUpload)
}
