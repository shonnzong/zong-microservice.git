package main

import (
	"github.com/gin-gonic/gin"
	"zong-microservice/config"
	c "zong-microservice/pkg/config"
	"zong-microservice/pkg/db"
	"zong-microservice/pkg/logger"
	"zong-microservice/pkg/redis"
	"zong-microservice/pkg/route"
)

func init() {
	// 初始化配置文件
	config.Initialize()
}

func main() {
	// 加载Gin框架
	app := gin.Default()
	// 注册路由
	route.RegisterRouter(app)
	// 初始化 SQL
	db.InitDB()
	// 初始化 Redis
	redis.InitRedis()
	// 执行任务调度器
	//console.Commands()
	// 初始化 websocket 服务
	//go ws.Manager.Start()
	// 启动服务
	err := app.Run(c.Get("app", "url") + ":" + c.Get("app", "port"))
	logger.LogError(err)
}
