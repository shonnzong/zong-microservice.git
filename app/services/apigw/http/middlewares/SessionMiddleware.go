package middlewares

import (
	"github.com/gin-gonic/gin"
	"zong-microservice/pkg/session"
)

// StartSession 强制标头返回 HTML 内容类型
func StartSession() gin.HandlerFunc {
	return func(context *gin.Context) {
		// 1. 启动会话
		session.StartSession(context)

		// 2. 继续处理接下去的请求
		context.Next()
	}
}
