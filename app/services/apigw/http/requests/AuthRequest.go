package request

type AuthLoginRequest struct {
	Username string `json:"username" binding:"required,max=50"`
	Password string `json:"password" binding:"required,max=100"`
}

//type AuthLogoutRequest struct {
//	//ID uint64 `json:"id" binding:"required"`
//}
//
//type AuthRefreshRequest struct {
//}
