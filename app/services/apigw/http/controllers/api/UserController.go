package controller

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"net/http"
	"zong-microservice/app/models"
	"zong-microservice/app/services/apigw/handlers"
	request "zong-microservice/app/services/apigw/http/requests"
	"zong-microservice/pkg/config"
	"zong-microservice/pkg/response"
)

type UserController struct {
	createRequest   request.UserCreateRequest
	updateRequest   request.UserUpdateRequest
	deleteRequest   request.UserDeleteRequest
	indexRequest    request.UserIndexRequest
	passwordRequest request.UserPasswordRequest
	registerRequest request.UserRegisterRequest
	model           models.User
	client          handlers.UserHandler
}

// Create 创建用户
func (user *UserController) Create(context *gin.Context) () {
	if err := context.ShouldBindJSON(&user.createRequest); err != nil {
		response.ResponseBadRequest(context)
		return
	}

	if result, err := user.client.RpcCreate(user.createRequest); err != nil {
		response.ResponseServerError(context, err.Error())
		return
	} else {
		if err = json.Unmarshal([]byte(result.Data), &user.model); err != nil {
			response.ResponseServerError(context, err.Error())
		}
		response.ResponseAuto(context, result.Code, result.Message, user.model)
		return
	}
}

// Update 更新用户
func (user *UserController) Update(context *gin.Context) () {
	if err := context.ShouldBindJSON(&user.updateRequest); err != nil {
		response.ResponseBadRequest(context)
		return
	}

	if result, err := user.client.RpcUpdate(user.updateRequest); err != nil {
		response.ResponseServerError(context, err.Error())
		return
	} else {
		response.ResponseAuto(context, result.Code, result.Message, result.Data)
		return
	}
}

// Delete 删除用户
func (user *UserController) Delete(context *gin.Context) () {
	if err := context.ShouldBindJSON(&user.deleteRequest); err != nil {
		response.ResponseBadRequest(context)
		return
	}

	if result, err := user.client.RpcDelete(user.deleteRequest); err != nil {
		response.ResponseServerError(context, err.Error())
		return
	} else {
		response.ResponseAuto(context, result.Code, result.Message, result.Data)
		return
	}
}

// List 用户列表
func (user UserController) List(context *gin.Context) {
	if err := context.ShouldBindQuery(&user.indexRequest); err != nil {
		response.ResponseBadRequest(context)
		return
	}

	page := context.Query(config.Get("pagination", "page_param"))
	baseUrl := config.Get("app", "url") + context.FullPath()
	if result, err := user.client.RpcList(user.indexRequest, 10, page, baseUrl); err != nil {
		response.ResponseServerError(context, err.Error())
		return
	} else {
		var apiUser = []models.ApiUser{}
		if err = json.Unmarshal([]byte(result.Data), &apiUser); err != nil {
			response.ResponseServerError(context, err.Error())
		}
		response.ResponseAuto(context, result.Code, result.Message, apiUser)
		return
	}
}

// Me 用户当前信息
func (user UserController) Me(context *gin.Context) () {
	authorization := context.Request.Header.Get("Authorization")

	if result, err := user.client.RpcMe(authorization); err != nil {
		response.ResponseServerError(context, err.Error())
		return
	} else {
		if err = json.Unmarshal([]byte(result.Data), &user.model); err != nil {
			response.ResponseServerError(context, err.Error())
		}
		response.ResponseAuto(context, result.Code, result.Message, user.model)
		return
	}
}

// UpdatePassword 用户修改密码
func (user *UserController) UpdatePassword(context *gin.Context) () {
	if err := context.ShouldBindQuery(&user.passwordRequest); err != nil {
		response.ResponseBadRequest(context)
		return
	}

	if result, err := user.client.RpcUpdatePassword(user.passwordRequest); err != nil {
		response.ResponseServerError(context, err.Error())
		return
	} else {
		if err = json.Unmarshal([]byte(result.Data), &user.model); err != nil {
			response.ResponseServerError(context, err.Error())
		}
		response.ResponseAuto(context, result.Code, result.Message, user.model)
		return
	}
}

// DoRegister 用户注册
func (user *UserController) DoRegister(context *gin.Context) {
	if err := context.ShouldBindJSON(&user.registerRequest); err != nil {
		response.ResponseBadRequest(context)
		return
	}

	if result, err := user.client.RpcDoRegister(user.registerRequest); err != nil {
		response.ResponseServerError(context, err.Error())
		return
	} else {
		if err = json.Unmarshal([]byte(result.Data), &user.model); err != nil {
			response.ResponseServerError(context, err.Error())
		}
		response.ResponseAuto(context, result.Code, result.Message, user.model)
		return
	}
}

// Index 用户登录页面
func (user *UserController) Index(context *gin.Context) {
	//context.Redirect(http.StatusFound, "signup.html")
	context.HTML(http.StatusOK, "signin.html", gin.H{})
}

// Register 用户注册页面
func (user *UserController) Register(context *gin.Context) {
	//context.Redirect(http.StatusFound, "signup.html")
	context.HTML(http.StatusOK, "signup.html", gin.H{})
}
