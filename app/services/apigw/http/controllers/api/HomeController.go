package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type HomeController struct {

}

// Index 主页面
func (home *HomeController) Index(context *gin.Context) {
	//context.Redirect(http.StatusFound, "signup.html")
	context.HTML(http.StatusOK, "home.html", gin.H{})
}