package controller

import (
	"github.com/gin-gonic/gin"
	"zong-microservice/pkg/ws"
)

type WsController struct {
	wss ws.WsServer
}

// Ws 连接 websocket
func (wsController *WsController) Ws(context *gin.Context) {
	wsController.wss.Server(context)
}
