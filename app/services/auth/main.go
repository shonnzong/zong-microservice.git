package main

import (
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-plugins/registry/consul/v2"
	"time"
	"zong-microservice/app/services/auth/handlers"
	"zong-microservice/app/services/auth/proto"
	"zong-microservice/config/consts"
	"zong-microservice/pkg/db"
	"zong-microservice/pkg/logger"
	"zong-microservice/pkg/redis"
)

func main() {
	// 初始化 SQL
	db.InitDB()
	// 初始化 Redis
	redis.InitRedis()

	// Register consul
	reg := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"127.0.0.1:8500",
			"127.0.0.1:8501",
			"127.0.0.1:8502",
		}
	})

	// Create service
	service := micro.NewService(
		micro.Name("go.micro.service.auth"),
		micro.Registry(reg),
		micro.RegisterTTL(time.Second*10),
		micro.RegisterInterval(time.Second*5),
		micro.Flags(consts.MicroFlags...),
	)

	// Init service, 解析命令行参数等
	service.Init()

	// Register handler
	err := proto.RegisterAuthServiceHandler(service.Server(), new(handlers.AuthHandler))
	logger.LogError(err)
	// Run service
	err = service.Run()
	logger.LogError(err)
}
