package consts

import "github.com/micro/cli/v2"

// micro flags
var MicroFlags = []cli.Flag{
	&cli.StringFlag{
		Name:  "db_host",
		Value: "127.0.0.1",
		Usage: "database address",
	},
	&cli.StringFlag{
		Name:  "mq_host",
		Value: "127.0.0.1",
		Usage: "mq(rabbitmq) address",
	},
	&cli.StringFlag{
		Name:  "cache_host",
		Value: "127.0.0.1",
		Usage: "cache(redis) address",
	},
	&cli.StringFlag{
		Name:  "ceph_host",
		Value: "127.0.0.1",
		Usage: "ceph address",
	},
}
