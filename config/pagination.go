package config

import "zong-microservice/pkg/config"

func init() {
	// 默认每页条数
	config.Set("pagination", "per_page", "10")
	// URL 中用以分辨多少页的参数
	config.Set("pagination", "page_param", "p")
}
