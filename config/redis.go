package config

import "zong-microservice/pkg/config"

func init() {
	// redis 服务地址
	config.Set("redis", "addr", config.Env("", "REDIS_ADDR", "localhost:6379"))
	// redis 服务密码
	config.Set("redis", "password", config.Env("", "REDIS_PASSWORD", ""))
	// redis 服务数据库
	config.Set("redis", "db", config.Env("", "REDIS_DB", "0"))
}
