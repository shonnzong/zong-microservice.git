package config

import "zong-microservice/pkg/config"

func init() {
	// session 驱动
	config.Set("session", "driver", config.Env("", "SESSION_DRIVER", "cookie"))
	// session 名称
	config.Set("session", "name", config.Env("", "SESSION_NAME", "zong-microservice"))
}
