package config

import "zong-microservice/pkg/config"

func init() {
	// 存储驱动
	config.Set("storage", "store", config.Env("", "STORAGE_STORE", "LOCAL"))
	// 是否开启文件异步转移(默认同步)
	config.Set("storage", "async_transfer", config.Env("", "ASYNC_TRANSFER_ENABLE", "false"))

	switch config.Get("storage", "store") {
	case "LOCAL": // 本地存储
		// 本地存储地址
		config.Set("storage", "path", config.Env("", "STORAGE_PATH", "storage/app/local/"))
		// 分块文件本地存储地址
		config.Set("storage", "part_path", config.Env("", "STORAGE_PART_PATH", "storage/app/local/part/"))
	case "CEPH": // Ceph存储
		// Ceph存储地址
		config.Set("storage", "path", config.Env("", "STORAGE_PATH", "/ceph"))
		config.Set("storage", "access_key", config.Env("", "ACCESS_KEY", "PFEA7NXWXSOWVTFA16C9"))
		config.Set("storage", "secret_Key", config.Env("", "SECRET_KEY", "cf3dwPMeadGbtEgwFUEA6emRVrVfDHpv0pLXFYby"))
		config.Set("storage", "endpoint", config.Env("", "ENDPOINT", "http://<你的rgw_host>:<<你的rgw_port>>"))
	case "OSS": // OSS存储
		// OSS存储地址
		config.Set("storage", "path", config.Env("", "STORAGE_PATH", "oss/"))
		config.Set("storage", "bucket", config.Env("", "BUCKET", "zong-microservice"))
		config.Set("storage", "access_key", config.Env("", "ACCESS_KEY", "<你的AccesskeyId>"))
		config.Set("storage", "secret_Key", config.Env("", "SECRET_KEY", "<你的AccessKeySecret>"))
		config.Set("storage", "endpoint", config.Env("", "ENDPOINT", "oss-cn-shenzhen.aliyuncs.com"))
	}

}
