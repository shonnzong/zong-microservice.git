package config

import "zong-microservice/pkg/config"

func init() {
	// rabbitmq服务的入口url
	config.Set("rabbitmq", "url", config.Env("", "RABBITMQ_URL", "amqp://guest:guest@127.0.0.1:5672/"))
	// 用于文件transfer的交换机
	config.Set("rabbitmq", "trans_exchange_name", config.Env("", "TRANS_EXCHANGE_NAME", "zong-microservice.trans"))
	// 转移队列名
	config.Set("rabbitmq", "trans_queue_name", config.Env("", "TRANS_QUEUE_NAME", "zong-microservice.trans.queue"))
	// 转移失败后写入另一个队列的队列名
	config.Set("rabbitmq", "trans_err_queue_name", config.Env("", "TRANS_ERR_QUEUE_NAME", "zong-microservice.trans.err-queue"))
	// routingkey
	config.Set("rabbitmq", "trans_routing_key", config.Env("", "TRANS_ROUTING_KEY", "zong-microservice"))
}
