@echo off
echo=

echo Build Start!

rd /s/q %cd%\bin\

if not exist %cd%\bin\config\ md %cd%\bin\config\
xcopy /Y %cd%\config\app.env %cd%\bin\config\

if not exist %cd%\bin\resources\ md %cd%\bin\resources\
xcopy /Y /S %cd%\resources %cd%\bin\resources\

xcopy /Y %cd%\.env %cd%\bin\

if \"%1\"==\"\" (
    xcopy /Y /S %cd%\shell %cd%\bin\
    set GOOS=windows
    set GOARCH=amd64
    echo build windows package
)
if \"%1\"==\"windows\" (
    xcopy /Y /S %cd%\shell %cd%\bin\
    set GOOS=windows
    set GOARCH=amd64
    echo build windows package
)
if \"%1\"==\"linux\" (
    set GOOS=linux
    set GOARCH=amd64
    echo build linux package
)
go build -o bin/main.exe main.go

echo :) Build Complete!

echo=
pause