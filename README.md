# zong-microservice

#### 介绍
基于Gin框架，使用go-micro微服务框架，自己封装的框架
框架集成了consul集群、mysql集群、ceph集群等

#### 软件架构
软件架构说明


#### 安装教程

1.  安装框架: git clone https://gitee.com/shonnzong/zong-microservice.git
2.  配置系统环境: 系统环境文件 .env , 需要做好自己的配置
3.  安装依赖组件: go mod vendor

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
