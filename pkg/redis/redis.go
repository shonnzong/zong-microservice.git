package redis

import (
	"github.com/go-redis/redis"
	"zong-microservice/pkg/config"
	"zong-microservice/pkg/helper"
	"zong-microservice/pkg/logger"
)

var Redis *redis.Client

// initRedis 初始化Redis
func InitRedis() {
	Redis = redis.NewClient(&redis.Options{
		Addr:     config.Get("redis", "addr"),
		Password: config.Get("redis", "password"),
		DB:       helper.StringToInt(config.Get("redis", "db")),
	})

	//Background返回一个非空的Context。 它永远不会被取消，没有值，也没有期限。
	//它通常在main函数，初始化和测试时使用，并用作传入请求的顶级上下文。
	//var ctx = context.Background()
	if _, err := Redis.Ping().Result(); err != nil {
		logger.LogError(err)
	}
}
