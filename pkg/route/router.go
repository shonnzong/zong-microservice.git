package route

import (
	"github.com/gin-gonic/gin"
	"zong-microservice/app/services/apigw/routes"
)

func RegisterRouter(engine *gin.Engine) {
	// 注册WEB路由
	//routes.RegisterWebRoutes(engine)
	// 注册API路由
	routes.RegisterApiRoutes(engine)
}
