package cron

import (
	"fmt"
	"time"
)

type CronProcess struct {
	ProcessTimer  *time.Timer
	ProcessFunc   []func() string
	ProcessPeriod time.Duration
}

// 定时器例子，达到 断续器 的效果，周而复始执行
// Start 开始定时器
func (cron *CronProcess) Start() {
	//t := time.Second * 10

	for {
		if cron.ProcessTimer == nil {
			cron.ProcessTimer = time.NewTimer(cron.ProcessPeriod)
		} else {
			// 通过 timer.Reset 函数重置这个定时器，达到目的
			// 将超时时间重置
			cron.ProcessTimer.Reset(cron.ProcessPeriod)
		}

		// 堵塞，谁有数据，就执行谁
		// 只会执行一次
		// 如果配合 for 死循环，可以达到一直监听管道的目的
		select {
		case <-cron.ProcessTimer.C:
			if len(cron.ProcessFunc) > 0 {
				for _, f := range cron.ProcessFunc {
					msg := f()
					fmt.Println(msg)
				}
			}
			//fmt.Printf("执行任务，统计小区数据：%v\n", 0)
			//fmt.Printf("执行任务，当前时间：%v\n", time.Now().Format("2021-05-20 13:14:00"))
		}
	}
}

// Stop 停止定时器
func (cron *CronProcess) Stop() {
	cron.ProcessTimer.Stop()
}
