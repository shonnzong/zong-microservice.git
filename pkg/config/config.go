package config

import (
	"zong-microservice/pkg/logger"
	"github.com/Unknwon/goconfig"
)

// Load 加载env环境变量
func LoadEnv() (c *goconfig.ConfigFile, err error) {
	// 1.加载配置文件
	config, err := goconfig.LoadConfigFile(".env")
	logger.LogError(err)
	return config, err
}

// Load 加载全局环境变量
func LoadGlobal() (c *goconfig.ConfigFile, err error) {
	// 1.加载配置文件
	config, err := goconfig.LoadConfigFile("config/app.env")
	logger.LogError(err)
	return config, err
}

// Env 获取环境变量，支持默认值
func Env(envSection string, envName string, defaultValue ...string) string {
	// 不存在的情况
	if config, err := LoadEnv(); err != nil {
		if len(defaultValue) > 0 {
			return defaultValue[0]
		}
		return ""
	} else {
		value, err := config.GetValue(envSection, envName)
		//logger.LogError(err)
		if err != nil && len(defaultValue) > 0 {
			return defaultValue[0]
		}
		return value
	}
}

// Get 获取配置项，支持默认值
func Get(section string, key string, defaultValue ...string) string {
	// 不存在的情况
	if config, err := LoadGlobal(); err != nil {
		if len(defaultValue) > 0 {
			return defaultValue[0]
		}
		return ""
	} else {
		value, err := config.GetValue(section, key)
		//logger.LogError(err)
		if err != nil && len(defaultValue) > 0 {
			return defaultValue[0]
		}
		return value
	}
}

// Set 新增配置项
func Set(section string, key string, value string) {
	if config, err := LoadGlobal(); err == nil {
		config.SetValue(section, key, value)
		Save(config)
	}
}

// Save 保存配置文件
func Save(config *goconfig.ConfigFile) {
	err := goconfig.SaveConfigFile(config, "config/app.env")
	logger.LogError(err)
}

// Reload 重载配置文件
func Reload(config *goconfig.ConfigFile) {
	err := config.Reload()
	logger.LogError(err)
}
