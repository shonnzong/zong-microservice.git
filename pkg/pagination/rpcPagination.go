package pagination

import (
	"gorm.io/gorm"
	"strings"
	"zong-microservice/pkg/config"
	"zong-microservice/pkg/helper"
)

// initRpcPagination 初始化rpc分页器
func initRpcPagination(db *gorm.DB, PerPage int, Page, BaseUrl string) *baseObject {
	// 默认每页数量
	if PerPage <= 0 {
		PerPage = helper.StringToInt(config.Get("pagination", "per_page"))
	}
	// 实例对象
	p := &baseObject{
		db:      db,
		PerPage: PerPage,
		Page:    1,
		Count:   -1,
	}
	// 设置 Page
	p.SetRpcPage(Page)
	// 设置 BaseUrl、PathUrl
	p.SetRpcUrl(BaseUrl)

	return p
}

// RpcPagination rpc分页对象构建器
// context —— 用来获取分页的 URL 参数，默认是 page，可通过 config/pagination.go 修改
// db —— GORM 查询句柄，用以查询数据集和获取数据总数
// PerPage —— 每页条数，传参为小于或者等于 0 时为默认值  10，可通过 config/pagination.go 修改
func RpcPagination(db *gorm.DB, data interface{}, PerPage int, Page, BaseUrl string) (PaginationObject, error) {
	// 初始化分页器
	p := initRpcPagination(db, PerPage, Page, BaseUrl)
	// 设置 Count
	p.TotalCount()
	result := PaginationObject{
		CurrentPage:  p.CurrentPage(),  // 当前页
		FirstPageURL: p.FirstPageURL(), // 第一页URL
		From:         p.From(),         // 开始页码
		LastPage:     p.LastPage(),     // 最后一页
		LastPageUrl:  p.LastPageUrl(),  // 最后一页URL
		NextPageUrl:  p.NextPageUrl(),  // 下一页URL
		Path:         p.Path(),         // URL地址
		PerPage:      p.PerPage,        // 每页条数
		PrevPageUrl:  p.PrevPageUrl(),  // 上一页URL
		To:           p.To(),           // 结束页码
		Total:        p.Count,          // 总条数
	}
	// 获取数据
	err := p.Results(data)
	result.Data = data

	return result, err
}

// SetRpcPage 设置 prc page 参数
func (p *baseObject) SetRpcPage(page string) {
	if page == "" {
		p.Page = 1
	} else {
		pageInt := helper.StringToInt(page)
		if pageInt <= 0 {
			p.Page = 1
		} else {
			p.Page = pageInt
		}
	}
}

// SetRpcUrl 设置 rpc URL 参数
func (p *baseObject) SetRpcUrl(baseUrl string) {
	pageParam := config.Get("pagination", "page_param")
	if strings.Contains(baseUrl, "?") {
		p.BaseUrl = baseUrl + "&" + pageParam + "="
	} else {
		p.BaseUrl = baseUrl + "?" + pageParam + "="
	}
	p.PathUrl = baseUrl
}
