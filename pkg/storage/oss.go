package storage

import (
	c "zong-microservice/pkg/config"
	"zong-microservice/pkg/logger"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

var ossCli *oss.Client

// OssClient : 创建oss client对象
func OssClient() *oss.Client {
	if ossCli != nil {
		return ossCli
	}
	ossCli, err := oss.New(c.Get("storage", "endpoint"),
		c.Get("storage", "access_key"), c.Get("storage", "secret_Key"))
	if err != nil {
		logger.LogError(err)
		return nil
	}
	return ossCli
}

// OssBucket : 获取bucket存储空间
func OssBucket() *oss.Bucket {
	cli := OssClient()
	if cli != nil {
		bucket, err := cli.Bucket(c.Get("storage", "bucket"))
		if err != nil {
			logger.LogError(err)
			return nil
		}
		return bucket
	}
	return nil
}

// OssDownloadURL : 临时授权下载url
func OssDownloadURL(objName string) string {
	signedURL, err := OssBucket().SignURL(objName, oss.HTTPGet, 3600)
	if err != nil {
		logger.LogError(err)
		return ""
	}
	return signedURL
}

// OssBuildLifecycleRule : 针对指定bucket设置生命周期规则
func OssBuildLifecycleRule(bucketName string) {
	// 表示前缀为test的对象(文件)距最后修改时间30天后过期。
	ruleTest1 := oss.BuildLifecycleRuleByDays("rule1", "test/", true, 30)
	rules := []oss.LifecycleRule{ruleTest1}

	err := OssClient().SetBucketLifecycle(bucketName, rules)
	logger.LogError(err)
}

// OssGenFileMeta :  构造文件元信息
func OssGenFileMeta(metas map[string]string) []oss.Option {
	options := []oss.Option{}
	for k, v := range metas {
		options = append(options, oss.Meta(k, v))
	}
	return options
}
