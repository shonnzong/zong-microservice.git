package helper

import (
	"encoding/json"
	"zong-microservice/pkg/logger"
	"strconv"
)

// Int64ToString 将 int64 转换为 string
func Int64ToString(num int64) string {
	return strconv.FormatInt(num, 10)
}

// Uint64ToString 将 uint64 转换为 string
func Uint64ToString(num uint64) string {
	return strconv.FormatUint(num, 10)
}

// StringToInt 将字符串转换为 int
func StringToInt(str string) int {
	i, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}
	return i
}

// InterfaceToJson 将 interface 转换为 json 字符串
func InterfaceToJson(i interface{}) string {
	jsons, err := json.Marshal(i)
	if err != nil {
		return ""
	}
	return string(jsons)
}

// JsonToInterface 将 json 字符串转换为 interface
func JsonToInterface(s []byte) interface{} {
	var result interface{}
	err := json.Unmarshal(s, &result)
	if err != nil {
		return nil
	}
	return result
}

// JsonToMapString 将 json 字符串转换为 map
func JsonToMapString(s []byte) map[string]interface{} {
	var result map[string]interface{}
	err := json.Unmarshal(s, &result)
	if err != nil {
		return nil
	}
	return result
}

// JsonToSliceMapString 将 json 字符串转换为 []map
func JsonToSliceMapString(s []byte) []map[string]interface{} {
	var result []map[string]interface{}
	err := json.Unmarshal(s, &result)
	if err != nil {
		return nil
	}
	return result
}

// StructToJson 将 struct 转换为 json 字符串
func StructToJson(i interface{}) string {
	jsons, err := json.Marshal(i)
	if err != nil {
		return ""
	}
	return string(jsons)
}

// JsonToStruct 将 json 字符串转换为 struct
func JsonToStruct(s []byte, ss interface{}) {
	err := json.Unmarshal(s, ss)
	logger.LogError(err)
}
