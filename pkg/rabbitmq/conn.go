package rabbitmq

import (
	"github.com/streadway/amqp"
	"log"
	c "zong-microservice/pkg/config"
	"zong-microservice/pkg/logger"
)

var conn *amqp.Connection
var channel *amqp.Channel

// 如果异常关闭，会接收通知
var notifyClose chan *amqp.Error

// UpdateRabbitMqHost : 更新mq host
func UpdateRabbitMqHost(host string) {
	c.Set("rabbitmq", "url", host)
}

// Init : 初始化MQ连接信息
func Init() {
	// 是否开启异步转移功能，开启时才初始化rabbitMQ连接
	if c.Get("storage", "async_transfer") == "false" {
		return
	}
	if initChannel(c.Get("rabbitmq", "url")) {
		channel.NotifyClose(notifyClose)
	}
	// 断线自动重连
	go func() {
		for {
			select {
			case msg := <-notifyClose:
				conn = nil
				channel = nil
				log.Printf("onNotifyChannelClosed: %+v\n", msg)
				initChannel(c.Get("rabbitmq", "url"))
			}
		}
	}()
}

func initChannel(rabbitHost string) bool {
	if channel != nil {
		return true
	}

	conn, err := amqp.Dial(rabbitHost)
	if err != nil {
		logger.LogError(err)
		return false
	}

	channel, err = conn.Channel()
	if err != nil {
		logger.LogError(err)
		return false
	}
	return true
}
